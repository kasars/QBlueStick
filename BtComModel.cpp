/* ============================================================
 *
 * Copyright (C) 2018 by Kåre Särs <kare.sars@iki .fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#include "BtComModel.h"

#include <QVector>
#include <QTimer>
#include <QBluetoothServiceDiscoveryAgent>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothSocket>
#include <QBluetoothLocalDevice>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QDebug>

#ifdef Q_OS_ANDROID
#include <QtAndroidExtras/QtAndroid>
#include <QtAndroidExtras/QAndroidJniEnvironment>
#include <QtAndroidExtras/QAndroidJniObject>
#include <QAndroidJniEnvironment>
QAndroidJniEnvironment jniEnv;
#endif

class BtComModel::Private {
public:
    enum Commands {
        CmdJoystick,
        CmdRequestSetup,
        CmdCustomData,
    };
    Q_ENUM(Commands)

    Private(BtComModel *q_): q(q_) {
        axis.resize(4);
        btnText1 = "1";
        btnText2 = "2";
        btnText3 = "3";
        btnText4 = "4";
        btnText5 = "5";
        btnText6 = "6";
        btnText7 = "7";
        btnText8 = "8";
    }

    void sendStatus() {
        if (!socket) {
            qDebug() << "No socket";
            return;
        }
        QByteArray writeData;
        writeData.resize(8);
        writeData[0] = 0x80;
        writeData[1] = 0x00;
        writeData[2] = CmdJoystick;
        writeData[3] = axis[0];
        writeData[4] = axis[1];
        writeData[5] = axis[2];
        writeData[6] = axis[3];
        writeData[7] = buttons;
        socket->write(writeData);
    }

    void requestSetup() {
        if (!socket) {
            qDebug() << "No socket";
            return;
        }
        QByteArray writeData;
        writeData.resize(3);
        writeData[0] = 0x80;
        writeData[1] = 0x00;
        writeData[2] = CmdRequestSetup;
        socket->write(writeData);
    }


    void addService(const QBluetoothServiceInfo &serviceInfo) {
        qDebug() << "Discovered service on" << serviceInfo.device().name() << serviceInfo.device().address().toString();
        qDebug() << "\tService name:" << serviceInfo.serviceName();
        qDebug() << "\tDescription:"  << serviceInfo.attribute(QBluetoothServiceInfo::ServiceDescription).toString();
        qDebug() << "\tProvider:"     << serviceInfo.attribute(QBluetoothServiceInfo::ServiceProvider).toString();
        qDebug() << "\tL2CAP protocol service multiplexer:" << serviceInfo.protocolServiceMultiplexer();
        qDebug() << "\tRFCOMM server channel:" << serviceInfo.serverChannel();

        serviceInfos << serviceInfo;
        devices <<  serviceInfo.device().name();
        deviceAddrs << serviceInfo.device().address().toString();
    }

    void readSocket() {
        if (!socket) {
            qDebug() << "No socket";
            return;
        }
        while (socket->canReadLine()) {
            QByteArray btData = socket->readLine();

            while (btData.endsWith('\r') || btData.endsWith('\n')) {
                btData.remove(btData.size()-1, 1);
            }
            emit q->dataReceived(btData);

            if (btData.startsWith("{ \"button1")) {
                QJsonDocument confDoc(QJsonDocument::fromJson(btData));
                QJsonObject root = confDoc.object();

                btnText1 = root["button1"].toString();
                btnText2 = root["button2"].toString();
                btnText3 = root["button3"].toString();
                btnText4 = root["button4"].toString();
                btnText5 = root["button5"].toString();
                btnText6 = root["button6"].toString();
                btnText7 = root["button7"].toString();
                btnText8 = root["button8"].toString();

                axisState1 = BtComModel::Normal;
                axisState2 = BtComModel::Normal;
                axisState3 = BtComModel::Normal;
                axisState4 = BtComModel::Normal;
                if (root["axis1"].toString() == "d") axisState1 = BtComModel::Disabled;
                if (root["axis2"].toString() == "d") axisState2 = BtComModel::Disabled;
                if (root["axis3"].toString() == "d") axisState3 = BtComModel::Disabled;
                if (root["axis4"].toString() == "d") axisState4 = BtComModel::Disabled;
                if (root["axis1"].toString() == "t") axisState1 = BtComModel::Turn;
                if (root["axis2"].toString() == "t") axisState2 = BtComModel::Turn;
                if (root["axis3"].toString() == "t") axisState3 = BtComModel::Turn;
                if (root["axis4"].toString() == "t") axisState4 = BtComModel::Turn;
                emit q->configChanged();
            }
        }
    }

    #ifdef Q_OS_ANDROID
    QMap<QString,QString> getKnownDevices()
    {
        QString serialportUuid = QBluetoothUuid(QBluetoothUuid::SerialPort).toString();
        serialportUuid.remove("{");
        serialportUuid.remove("}");
        QMap<QString,QString> result;
        /** Reference to Java object android.bluetooth.BluetoothAdapter */
        QAndroidJniObject adapter = QAndroidJniObject::callStaticObjectMethod("android/bluetooth/BluetoothAdapter",
                                                                              "getDefaultAdapter",
                                                                              "()Landroid/bluetooth/BluetoothAdapter;");
        QAndroidJniObject pairedDevicesSet = adapter.callObjectMethod("getBondedDevices", "()Ljava/util/Set;"); // Set<BluetoothDevice>
        jint size=pairedDevicesSet.callMethod<jint>("size");
        qDebug("Found %i paired devices",size);
        if (size>0) {
            QAndroidJniObject iterator=pairedDevicesSet.callObjectMethod("iterator", "()Ljava/util/Iterator;"); // Iterator<BluetoothDevice>
            for (int i=0; i<size; i++) {
                QAndroidJniObject dev = iterator.callObjectMethod("next", "()Ljava/lang/Object;"); // BluetoothDevice
                QString address = dev.callObjectMethod("getAddress", "()Ljava/lang/String;").toString();
                QString name = dev.callObjectMethod("getName", "()Ljava/lang/String;").toString();
                qDebug() << name << address;
                QAndroidJniObject uuids = dev.callObjectMethod("getUuids", "()[Landroid/os/ParcelUuid;");
                int arrayLength = jniEnv->GetArrayLength(uuids.object<jarray>());
                for (int j=0; j<arrayLength; ++j) {
                    QAndroidJniObject arrayElement = jniEnv->GetObjectArrayElement(uuids.object<jobjectArray>(), j);
                    qDebug() << arrayElement.toString();
                    if (arrayElement.toString() == serialportUuid) {
                        result.insert(address, name);
                        break;
                    }
                }
            }
        }
        return result;
    }
    #endif

    BtComModel *q;
    QByteArray axis;
    quint8 buttons = 0;
    QTimer sendTmr;
    QBluetoothLocalDevice            localDevice;
    QBluetoothServiceDiscoveryAgent *discoveryAgent = nullptr;
    QBluetoothDeviceDiscoveryAgent  *devDiscoveryAgent = nullptr;
    QBluetoothSocket                *socket = nullptr;
    bool                             connected = false;
    QStringList                      devices;
    QStringList                      deviceAddrs;
    QVector<QBluetoothServiceInfo>   serviceInfos;
    int                              connectingIndex=-1;

    QString btnText1;
    QString btnText2;
    QString btnText3;
    QString btnText4;
    QString btnText5;
    QString btnText6;
    QString btnText7;
    QString btnText8;

    int axisState1 = BtComModel::Normal;
    int axisState2 = BtComModel::Normal;
    int axisState3 = BtComModel::Normal;
    int axisState4 = BtComModel::Normal;
};

BtComModel::BtComModel(QObject *parent) : QObject(parent), d(new Private(this))
{
    connect(&d->sendTmr, &QTimer::timeout, this, [this]() {
        d->sendStatus();
    });

    d->discoveryAgent = new QBluetoothServiceDiscoveryAgent(this);

    connect(d->discoveryAgent, &QBluetoothServiceDiscoveryAgent::serviceDiscovered, this, [this](const QBluetoothServiceInfo &service) {
        d->addService(service);
    });


    d->devDiscoveryAgent = new QBluetoothDeviceDiscoveryAgent(this);
    connect(d->devDiscoveryAgent, &QBluetoothDeviceDiscoveryAgent::deviceDiscovered,
            this, [](const QBluetoothDeviceInfo &info) {
                qDebug() << info.name() << info.address().toString();
            });

    connect(d->discoveryAgent, &QBluetoothServiceDiscoveryAgent::finished, this, [this]() {
        qDebug() << "Scan done";
        emit devicesChanged();
        emit scanningChanged();
    });
    connect(d->discoveryAgent, &QBluetoothServiceDiscoveryAgent::canceled, this, [this]() {
        qDebug() << "Scan canceled";
        emit devicesChanged();
        emit scanningChanged();
    });
    connect(d->discoveryAgent, QOverload<QBluetoothServiceDiscoveryAgent::Error>::of(&QBluetoothServiceDiscoveryAgent::error),
            this, []() {
                qDebug() << "Service scan error!";
            });

    if (d->localDevice.isValid()) {
        qDebug() << d->localDevice.name();
        // Turn Bluetooth on
        d->localDevice.powerOn();

        // Make it visible to others
        //localDevice.setHostMode(QBluetoothLocalDevice::HostDiscoverable);

        scannForDevices();
    }
    else {
        qDebug() << "no bluetooth device found";
    }

}

BtComModel::~BtComModel()
{
    delete d;
}

bool BtComModel::connected() const
{
    return d->connected;
}

int BtComModel::connectingIndex() const
{
    return d->connectingIndex;
}

bool BtComModel::bluetoothAvailable() const
{
    return d->localDevice.isValid();
}

void BtComModel::scannForDevices()
{
    if (scanning()) {
        qDebug() << "Already scanning...";
        return;
    }

    d->devices.clear();
    d->deviceAddrs.clear();

#ifdef Q_OS_ANDROID
    QMap<QString,QString> devMap;
    devMap = d->getKnownDevices();
    qDebug() << devMap;

    auto i = devMap.constBegin();
    while (i != devMap.constEnd()) {
        d->devices += i.value();
        d->deviceAddrs += i.key();
        ++i;
    }

#else

    d->discoveryAgent->setUuidFilter(QBluetoothUuid(QBluetoothUuid::SerialPort));
    //d->discoveryAgent->start(QBluetoothServiceDiscoveryAgent::FullDiscovery);
    d->discoveryAgent->start(QBluetoothServiceDiscoveryAgent::MinimalDiscovery);
#endif

    emit devicesChanged();
    emit scanningChanged();
}

const QStringList BtComModel::devices() const
{
    return d->devices;
}

const QStringList BtComModel::deviceAddrs() const
{
    return d->deviceAddrs;
}

bool BtComModel::scanning() const
{
    return d->discoveryAgent->isActive();
}

void BtComModel::connectToService(int index)
{
    qDebug() << "Try connect to " << index;
    if (index < 0 || index >= d->devices.size()) {
        qDebug() << "Bad device index";
        return;
    }
    d->connectingIndex = index;
    emit connectedChanged();

    if (d->socket) {
        delete d->socket;
    }
    d->socket = new QBluetoothSocket(QBluetoothServiceInfo::RfcommProtocol);
    d->socket->connectToService(QBluetoothAddress(d->deviceAddrs[index]), QBluetoothUuid(QBluetoothUuid::SerialPort));

    connect(d->socket, &QBluetoothSocket::readyRead, this, [this]() { d->readSocket(); });
    connect(d->socket, &QBluetoothSocket::connected, this, [this]() {
        d->connectingIndex = -1;
        d->connected = true;
        emit connectedChanged();
        d->sendTmr.start(10);
        d->requestSetup();
        qDebug() << "Server Connected";
    });
    connect(d->socket, &QBluetoothSocket::disconnected, this, [this]() {
        d->connectingIndex = -1;
        d->connected = false;
        emit connectedChanged();
        d->sendTmr.stop();
        qDebug() << "Server Disconnected";
    });
    connect(d->socket, QOverload<QBluetoothSocket::SocketError>::of(&QBluetoothSocket::error), this,
    [this](QBluetoothSocket::SocketError error) {
        d->connectingIndex = -1;
        emit connectedChanged();
        d->sendTmr.stop();
        qDebug() << "Socket error:" << error;
    });

    connect(d->socket, &QBluetoothSocket::stateChanged, this, [](QBluetoothSocket::SocketState state) {
        qDebug() << "Socket stateChanged:" << state;
    });
}


QString BtComModel::btnText1() const { return d->btnText1; }
QString BtComModel::btnText2() const { return d->btnText2; }
QString BtComModel::btnText3() const { return d->btnText3; }
QString BtComModel::btnText4() const { return d->btnText4; }
QString BtComModel::btnText5() const { return d->btnText5; }
QString BtComModel::btnText6() const { return d->btnText6; }
QString BtComModel::btnText7() const { return d->btnText7; }
QString BtComModel::btnText8() const { return d->btnText8; }

int BtComModel::axisState1() const { return d->axisState1; }
int BtComModel::axisState2() const { return d->axisState2; }
int BtComModel::axisState3() const { return d->axisState3; }
int BtComModel::axisState4() const { return d->axisState4; }




/** This function is used to update an axis position. The function takes a float
 * value between -1.0 and 10, but the value will be translated into a signed byte
 * value between -127 and 127.
 * @param axis is the axis to update
 * @param newPos is the floating-point value between -1.0 and 1.0 for the axis.
 * Any value outside this range will be ignored!
 */
void BtComModel::axesPosChanged(int axis, float newPos)
{
    if (axis < 0 || axis >= d->axis.size()) {
        qDebug() << "Bad axis" << axis;
        return;
    }
    if (newPos < -1 || newPos > 1) {
        qDebug() << "Bad position" << newPos << "for axis" << axis ;
        return;
    }
    d-> axis[axis] = (quint8)qRound(newPos * 127);
}

/** This function is used to set a button pressed.
 * @param button is the button index. The button index must be between 0 and 7
 */
void BtComModel::buttonPressed(int button)
{
    if (button < 0 || button > 7) {
        qDebug() << "Bad button" << button;
        return;
    }
    d->buttons |= 1<<button;
}

/** This function is used to set a button released.
 * @param button is the button index. The button index must be between 0 and 7
 */
void BtComModel::buttonReleased(int button)
{
    if (button < 0 || button > 7) {
        qDebug() << "Bad button" << button;
        return;
    }
    d->buttons &= ~(quint8)(1<<button);
}

