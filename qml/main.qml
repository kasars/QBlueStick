import QtQuick 2.3
import QtQuick.Controls 2.9
import QtQuick.Dialogs 1.2
import QBlueStick 1.0

ApplicationWindow {
    visible: true
    width: 1376
    height: 786
    title: qsTr("Bluetooth Joystick")
    Rectangle {
        anchors.fill: parent
        color: "#CCCCCC"
    }

    readonly property double pageMargins: height * 0.01

    Row {
        anchors {
            top: parent.top
            left: parent.left
            right: leftStick.right
            bottom: leftStick.top
            topMargin: pageMargins
            bottomMargin: pageMargins
        }
        spacing: pageMargins

        BtButton {
            height: parent.height
            width: (parent.width-pageMargins)/2
            font.pixelSize: height/4
            text: btCom.btnText1
            onPressedChanged: {
                if (pressed) btCom.buttonPressed(0);
                else btCom.buttonReleased(0);
            }
            visible: text !== ""
        }
        BtButton {
            height: parent.height
            width: (parent.width-pageMargins)/2
            font.pixelSize: height/4
            text: btCom.btnText2
            onPressedChanged: {
                if (pressed) btCom.buttonPressed(1);
                else btCom.buttonReleased(1);
            }
            visible: text !== ""
        }
    }

    Joystick {
        id: leftStick;
        anchors {
            left: parent.left
            verticalCenter: parent.verticalCenter
        }
        width: parent.width*0.4
        height: width

        xDisabled: btCom.axisState1 == BtComModel.Disabled
        yDisabled: btCom.axisState2 == BtComModel.Disabled
        useTurnArrows: btCom.axisState3 == BtComModel.Turn

        onXPossChanged: {
            btCom.axesPosChanged(0, xPoss);
        }
        onYPossChanged: {
            btCom.axesPosChanged(1, yPoss);
        }
    }

    Row {
        anchors {
            top: leftStick.bottom
            left: parent.left
            right: leftStick.right
            bottom: parent.bottom
            topMargin: pageMargins
            bottomMargin: pageMargins
        }
        spacing: pageMargins

        BtButton {
            height: parent.height
            width: (parent.width-pageMargins)/2
            font.pixelSize: height/4
            text: btCom.btnText3
            onPressedChanged: {
                if (pressed) btCom.buttonPressed(2);
                else btCom.buttonReleased(2);
            }
            visible: text !== ""
        }
        BtButton {
            height: parent.height
            width: (parent.width-pageMargins)/2
            font.pixelSize: height/4
            text: btCom.btnText4
            onPressedChanged: {
                if (pressed) btCom.buttonPressed(3);
                else btCom.buttonReleased(3);
            }
            visible: text !== ""
        }
    }

    TextArea {
        id: textArea
        anchors {
            top: parent.top
            left: leftStick.right
            right:rightStick.left
            bottom:parent.bottom
        }
        MouseArea { anchors.fill: parent }
        wrapMode: TextEdit.Wrap

        font.pixelSize: textArea.height / 50
    }

    Row {
        anchors {
            top: parent.top
            left: rightStick.left
            right: rightStick.right
            bottom: rightStick.top
            topMargin: pageMargins
            bottomMargin: pageMargins
        }
        spacing: pageMargins

        BtButton {
            height: parent.height
            width: (parent.width-pageMargins)/2
            font.pixelSize: height/4
            text: btCom.btnText5
            onPressedChanged: {
                if (pressed) btCom.buttonPressed(4);
                else btCom.buttonReleased(4);
            }
            visible: text !== ""
        }
        BtButton {
            height: parent.height
            width: (parent.width-pageMargins)/2
            font.pixelSize: height/4
            text: btCom.btnText6
            onPressedChanged: {
                if (pressed) btCom.buttonPressed(5);
                else btCom.buttonReleased(5);
            }
            visible: text !== ""
        }
    }


    Joystick {
        id: rightStick;
        anchors {
            right: parent.right
            verticalCenter: parent.verticalCenter
        }
        width: parent.width*0.4
        height: width

        xDisabled: btCom.axisState3 == BtComModel.Disabled
        yDisabled: btCom.axisState4 == BtComModel.Disabled
        useTurnArrows: btCom.axisState3 == BtComModel.Turn

        onXPossChanged: {
            btCom.axesPosChanged(2, xPoss);
        }
        onYPossChanged: {
            btCom.axesPosChanged(3, yPoss);
        }
    }

    Row {
        anchors {
            top: rightStick.bottom
            left: rightStick.left
            right: parent.right
            bottom: parent.bottom
            topMargin: pageMargins
            bottomMargin: pageMargins
        }
        spacing: pageMargins

        BtButton {
            height: parent.height
             width: (parent.width-pageMargins)/2
            font.pixelSize: height/4
            text: btCom.btnText7
            onPressedChanged: {
                if (pressed) btCom.buttonPressed(6);
                else btCom.buttonReleased(6);
            }
            visible: text !== ""
        }
        BtButton {
            height: parent.height
            width: (parent.width-pageMargins)/2
            font.pixelSize: height/4
            text: btCom.btnText8
            onPressedChanged: {
                if (pressed) btCom.buttonPressed(7);
                else btCom.buttonReleased(7);
            }
            visible: text !== ""
        }
    }

    ScanningView {
        anchors.centerIn: parent
        width: parent.width * 0.5
        height: parent.height * 0.6
        radius: pageMargins*2
        model: btCom.devices
        deviceAddrs: btCom.deviceAddrs
        scanning: btCom.scanning
        connectingIndex: btCom.connectingIndex

        onSelectItem: btCom.connectToService(index)
        onReScan: btCom.scannForDevices()

        visible: !btCom.connected
    }

    BtComModel {
        id: btCom

        onDataReceived: {
            textArea.append(data);

            while (textArea.lineCount > 40) {
                textArea.remove(0, 10);
            }
        }
    }
}
