/* ============================================================
 *
 * Copyright (C) 2018 by Kåre Särs <kare.sars@iki .fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#include <QCoreApplication>
#include "BtServer.h"


#include <QDebug>

BtServer::BtServer():
m_server(0), socket(0), m_serviceFound(false)
{
    startServer();
}

BtServer::~BtServer()
{
    delete m_server;
    delete socket;
}

void BtServer::startServer()
{
    qDebug() << "Starting the server";
    //! [Starting the server]
    m_server = new QBluetoothServer(QBluetoothServiceInfo::RfcommProtocol, this);
    connect(m_server, &QBluetoothServer::newConnection, this, &BtServer::clientConnected);
    connect(m_server, QOverload<QBluetoothServer::Error>::of(&QBluetoothServer::error), this, &BtServer::serverError);
    const QBluetoothUuid uuid(QBluetoothUuid::SerialPort);

    m_server->listen(uuid, QStringLiteral("BtServer server"));

    qDebug() << "serverStarted";
}

void BtServer::clientConnected()
{
    if (!m_server->hasPendingConnections()) {
        qDebug() << "FAIL: expected pending server connection";
        return;
    }
    socket = m_server->nextPendingConnection();
    if (!socket)
        return;
    socket->setParent(this);
    connect(socket, &QBluetoothSocket::readyRead,    this, &BtServer::readSocket);
    connect(socket, &QBluetoothSocket::disconnected, this, &BtServer::clientDisconnected);
    connect(socket, QOverload<QBluetoothSocket::SocketError>::of(&QBluetoothSocket::error), this, &BtServer::socketError);
    qDebug() << "Client connected.";
    socket->write("Wellcome!");

}

void BtServer::clientDisconnected()
{
    qDebug() << "Client disconnected";
}

void BtServer::socketError(QBluetoothSocket::SocketError error)
{
    qDebug() << error;
}

void BtServer::serverError(QBluetoothServer::Error error)
{
    qDebug() << error;
}


void BtServer::readSocket()
{
    if (!socket) {
        qDebug() << "No socket";
        return;
    }
    while (socket->bytesAvailable()) {
        QByteArray btData = socket->readAll();
        for (int i=0; i<btData.size(); ++i) {
            qDebug() << btData[i];
        }
    }
}

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    BtServer server;
    return app.exec();
}
