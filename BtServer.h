/* ============================================================
 *
 * Copyright (C) 2018 by Kåre Särs <kare.sars@iki .fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */
#ifndef BtServer_h
#define BtServer_h

#include <QObject>
#include <QBluetoothServer>
#include <QBluetoothServiceInfo>

class BtServer: public QObject
{
    Q_OBJECT
public:
    BtServer();
    ~BtServer();

public slots:
    void startServer();
    void clientConnected();
    void clientDisconnected();
    void socketError(QBluetoothSocket::SocketError);
    void serverError(QBluetoothServer::Error);
    void readSocket();


private:
    QBluetoothServer *m_server;
    QBluetoothServiceInfo m_serviceInfo;
    QBluetoothSocket *socket;
    bool m_serviceFound;
};

#endif // PINGPONG_H
