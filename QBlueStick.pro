TEMPLATE = app

QT += qml quick widgets bluetooth

android {
    QT += androidextras
}

CONFIG += c++11

SOURCES += main.cpp \
    BtComModel.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    BtComModel.h

DISTFILES += \
    android/AndroidManifest.xml \
    android/res/values/libs.xml

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

ANDROID_ABIS = armeabi-v7a arm64-v8a
